package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.Domain;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.DumpFileException;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    public static final String DESCRIPTION = "Load data from base46 dump file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA BASE64 LOAD]");
        try {
            @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BASE64));
            @Nullable final String base64Date = new String(base64Byte);
            @Nullable final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Date);
            @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();
            setDomain(domain);
        } catch (final Exception exception) {
            throw new DumpFileException(FILE_BASE64, exception);
        }
    }

}
