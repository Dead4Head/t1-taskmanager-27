package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.Domain;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.DumpFileException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public final class DataBinSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-bin";

    @NotNull
    public static final String DESCRIPTION = "Save data to binary dump file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE BINARY]");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BIN);
        @NotNull final Path path = file.toPath();

        try {
            Files.deleteIfExists(path);
            Files.createFile(path);

            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(domain);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (final Exception exception) {
            throw new DumpFileException(FILE_BIN, exception);
        }
    }

}
