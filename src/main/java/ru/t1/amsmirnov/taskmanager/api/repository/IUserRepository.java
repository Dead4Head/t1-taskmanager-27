package ru.t1.amsmirnov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findOneByLogin(@NotNull String login);

    @Nullable
    User findOneByEmail(@NotNull String email);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

}
